﻿
namespace 贪吃蛇小游戏
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Start = new System.Windows.Forms.Button();
            this.Game_Panel = new System.Windows.Forms.Panel();
            this.labScore = new System.Windows.Forms.Label();
            this.labLevel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_Start
            // 
            this.Btn_Start.BackColor = System.Drawing.Color.Silver;
            this.Btn_Start.Location = new System.Drawing.Point(41, 17);
            this.Btn_Start.Name = "Btn_Start";
            this.Btn_Start.Size = new System.Drawing.Size(120, 45);
            this.Btn_Start.TabIndex = 0;
            this.Btn_Start.Text = "开始游戏";
            this.Btn_Start.UseVisualStyleBackColor = false;
            this.Btn_Start.Click += new System.EventHandler(this.Btn_Start_Click);
            // 
            // Game_Panel
            // 
            this.Game_Panel.Location = new System.Drawing.Point(41, 82);
            this.Game_Panel.Name = "Game_Panel";
            this.Game_Panel.Size = new System.Drawing.Size(350, 358);
            this.Game_Panel.TabIndex = 1;
            // 
            // labScore
            // 
            this.labScore.AutoSize = true;
            this.labScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.labScore.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labScore.ForeColor = System.Drawing.Color.White;
            this.labScore.Location = new System.Drawing.Point(167, 31);
            this.labScore.Name = "labScore";
            this.labScore.Size = new System.Drawing.Size(66, 25);
            this.labScore.TabIndex = 2;
            this.labScore.Text = "分数:0";
            // 
            // labLevel
            // 
            this.labLevel.AutoSize = true;
            this.labLevel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.labLevel.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labLevel.ForeColor = System.Drawing.Color.White;
            this.labLevel.Location = new System.Drawing.Point(316, 31);
            this.labLevel.Name = "labLevel";
            this.labLevel.Size = new System.Drawing.Size(66, 25);
            this.labLevel.TabIndex = 3;
            this.labLevel.Text = "等级:0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 453);
            this.Controls.Add(this.labLevel);
            this.Controls.Add(this.labScore);
            this.Controls.Add(this.Game_Panel);
            this.Controls.Add(this.Btn_Start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button Btn_Start;
        private System.Windows.Forms.Panel Game_Panel;
        private System.Windows.Forms.Label labScore;
        private System.Windows.Forms.Label labLevel;
    }
}

