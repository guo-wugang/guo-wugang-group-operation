﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace 贪吃蛇小游戏
{
    public partial class Form1 : Form
    {
        //游戏控制器对象
        private Controller.GameController gameController;
        public Form1()
        {
            InitializeComponent();
            //在窗体类的构造函数中调用帮助类的构造方法画方格
            this.Game_Panel.Size = 工具.CommonHelper.InitializeEnvironment(this.Game_Panel.Width, this.Game_Panel.Height);
            //关闭跨线程访问检查
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Btn_Start_Click(object sender, EventArgs e)
        {
            //创建画家对象
            Graphics g = Graphics.FromHwnd(this.Game_Panel.Handle);
            //创建游戏控制器对象
            gameController = new Controller.GameController(g,this);
            //调用游戏控制器的开始游戏
            gameController.NewGame();
            Btn_Start.Enabled = false;

            //工具.Snake snake = new 工具.Snake();
            //snake.Draw(g);
            //Thread.Sleep(1000);
            //snake.AutoSnake(g);
        }
        //在键盘上按下任意一个键的时候，调用此方法
        protected override bool ProcessDialogKey(Keys keyData)
        {
            //将按键传递给控制器对象
            this.gameController.ProcessDialogKey(keyData);
            return base.ProcessDialogKey(keyData);
        }
        public void ShowScore()
        {
            labScore.Text = "分数："+ 工具.CommonHelper.Score;
            labLevel.Text = "等级：" + 工具.CommonHelper.Level;
        }
    }
}
