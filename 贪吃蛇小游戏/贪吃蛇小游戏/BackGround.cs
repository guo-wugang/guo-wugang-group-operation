﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using 贪吃蛇小游戏.工具;

namespace 贪吃蛇小游戏
{
    class BackGround
    {
        private GroundUnitType[,] blocks = new GroundUnitType[CommonHelper.RowCount, CommonHelper.ColCount];

        private RandomMaker maker = new RandomMaker();
        public void MakeAFood(Graphics g,Snake snake)
        {
        loop:
            //得到一个随机的坐标
            Position pos = maker.GetARandomPosition();
            //判断食物坐标是否在蛇的身体上
            foreach(SnakeUnit u in snake.body)
            {
                if(u.Pos.rowIndex == pos.rowIndex && u.Pos.colIndex == pos.colIndex)
                {
                    goto loop;
                }
            }
            //将二维数组中对应的元素的值改为Food
            blocks[pos.rowIndex, pos.colIndex] = GroundUnitType.Food;
            //将食物画出
            Brush brush = new SolidBrush(CommonHelper.FoodColor);
            g.FillRectangle(brush, new Rectangle(pos.colIndex * CommonHelper.Length + 1, pos.rowIndex * CommonHelper.Length + 1, CommonHelper.Length - 1, CommonHelper.Length - 1));

        }
        #region 绘制网格

        public void Draw(Graphics g)
        {
            for (int rowIndex = 0; rowIndex < CommonHelper.RowCount; rowIndex++)
            {
                for (int colIndex = 0; colIndex < CommonHelper.ColCount; colIndex++)
                {
                    //绘制网格
                    g.DrawRectangle(new Pen(CommonHelper.LineColor), new Rectangle(colIndex * CommonHelper.Length, rowIndex * CommonHelper.Length, CommonHelper.Length, CommonHelper.Length));
                    //填充网格颜色
                    g.FillRectangle(new SolidBrush(CommonHelper.GroundColor), new Rectangle(colIndex * CommonHelper.Length + 1, rowIndex * CommonHelper.Length + 1, CommonHelper.Length - 1, CommonHelper.Length - 1));
                }
            }
        } 
        #endregion

        ///<summary>
        ///判断蛇是否能够在背景移动
        /// </summary>
        public bool IsMoveable(Snake snake)
        {
            //判断的思路：取出蛇头的坐标，并判断其是否越界
            Position pos = snake.SnakeHead.Pos;

            //根据蛇头的前进方向确定下一个坐标
            switch (snake.SnakeHead.Direction)
            {
                case Direction.Left:pos.colIndex--;
                    break;
                case Direction.Up:pos.rowIndex--;
                    break;
                case Direction.Right:pos.colIndex++;
                    break;
                case Direction.Dowm:pos.rowIndex++;
                    break;
            }
            //判断蛇头下一个坐标是否越界
            if (pos.colIndex < 0 || pos.rowIndex < 0 || pos.colIndex >= CommonHelper.ColCount || pos.rowIndex >= CommonHelper.RowCount)
            {
                return false;
            }
                return true;
            //if(pos.colIndex == 0 && snake.SnakeHead.Direction != Direction.Left || pos.rowIndex < 0 || pos.colIndex > CommonHelper.ColCount - 1 || pos.rowIndex > CommonHelper.RowCount - 1)
            //{
            //    return false;
            //}
            //return true;
        }
        /// <summary>
        /// 判断蛇是否吃到食物
        /// </summary>
        /// <param name="snake"></param>
        /// <returns></returns>
        public bool IsEatFood(Snake snake)
        {
            //取出蛇头的坐标
            Position pos = snake.SnakeHead.Pos;
            //以蛇头的坐标当作二维数组的下标，取出对应元素的值
            GroundUnitType  type = blocks[pos.rowIndex, pos.colIndex];
            if(type == GroundUnitType.Food)
            {
                //将食物的坐标的值改为背景值
                blocks[pos.rowIndex, pos.colIndex] = GroundUnitType.Blank;
                return true;
            }
            return false;
        }
    }
}
