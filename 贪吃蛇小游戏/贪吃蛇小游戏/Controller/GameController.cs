﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using 贪吃蛇小游戏.工具;
using System.Threading;
using System.Windows.Forms;

namespace 贪吃蛇小游戏.Controller
{
    ///<summary>
    ///游戏控制器
    /// </summary>
    class GameController:Controller
    {
        //游戏控制器的背景对象
        private BackGround background;
        //游戏控制器的蛇对象
        private Snake snake;
        //初始化当前对象的背景字段和蛇字段
        private Graphics graphics;
        private Form1 mForm;
        public GameController(Graphics g,Form1 mForm)
        {
            this.mForm = mForm;
            this.graphics = g;
            this.background = new BackGround();
            this.snake = new Snake();
        }
        /// <summary>
        /// 开始新游戏的方法
        /// </summary>
        public void NewGame()
        {
            CommonHelper.Score = 0;
            CommonHelper.Level = 0;
            CommonHelper.Speed = 600;
            mForm.ShowScore();
            //把背景绘制出来
            this.background.Draw(graphics);
            //把蛇绘制出来
            this.snake.Draw(graphics);
            //产生食物
            this.background.MakeAFood(graphics,snake);
            //让蛇动起来
            //创建一个线程，并设置位后台线程，让蛇动起来
            Thread snakeMoveThread = new Thread(SnakeAutoMoveThread);
            snakeMoveThread.IsBackground = true;
            snakeMoveThread.Start();

        }
        private void SnakeAutoMoveThread()
        {
            while (background.IsMoveable(snake))
            {
                //判断蛇头是否吃到了自己
                if (snake.IsEatSelf())
                {
                    break;
                }
                //先判断蛇是否吃到到食物
                if (background.IsEatFood(snake))
                {
                    snake.AddBody();//增加身体
                    //再次随机产生食物
                    background.MakeAFood(graphics,snake);
                    CommonHelper.Score++;
                    if(CommonHelper.Score % 5 == 0)
                    {
                        CommonHelper.Level++;
                        CommonHelper.Speed -= 50;
                    }
                    mForm.ShowScore();
                }
                snake.AutoSnake(graphics);
                Thread.Sleep(CommonHelper.Speed);
            }
            //循环结束玩，游戏结束
            graphics.DrawString("Game Over", new Font("宋体", 30), Brushes.Yellow, new PointF(15,10));
            //将开始游戏按钮设置为可用
            mForm.Btn_Start.Enabled = true;
        }
        /// <summary>
        /// 处理用户传递过来的键值
        /// </summary>
        /// <param name="keyData"></param>
        public void ProcessDialogKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                    snake.SnakeHead.Direction = Direction.Up;
                    break;
                case Keys.Left:
                    snake.SnakeHead.Direction = Direction.Left;
                    break;
                case Keys.Right:
                    snake.SnakeHead.Direction = Direction.Right;
                    break;
                case Keys.Down:
                    snake.SnakeHead.Direction = Direction.Dowm;
                    break;
            }
        }
    }
}
