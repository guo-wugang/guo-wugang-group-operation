﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 贪吃蛇小游戏.工具
{
    struct Position
    {
        //行坐标
        public int rowIndex;
        //列坐标
        public int colIndex;
        public Position(int rowIndex,int colIndex)
        {
            this.rowIndex = rowIndex;
            this.colIndex = colIndex;
        }
    }
}
