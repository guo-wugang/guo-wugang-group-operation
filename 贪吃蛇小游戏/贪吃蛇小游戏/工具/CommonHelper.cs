﻿using System.Drawing;

namespace 贪吃蛇小游戏.工具
{
    static class CommonHelper
    {


        #region 游戏面板默认总行数14和总列数12
        private static int rowCount = 14;
        public static int RowCount { get => rowCount; set => rowCount = value; }
        private static int colCount = 12;
        public static int ColCount { get => colCount; set => colCount = value; }
        #endregion

        #region 小方格的边长
        private static int length;
        public static int Length { get => length; set => length = value; }


        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paneWidth">游戏面板宽度</param>
        /// <param name="paneHeight">游戏面板的高度</param>
        #region 初始化游戏方格
        public static Size InitializeEnvironment(int paneWidth, int paneHeight)
        {
            //根据游戏面板的宽高算出小方格的宽高
            int width = paneWidth / colCount;
            int height = paneHeight / rowCount;
            length = width > height ? height : width;
            Size size = new Size();
            size.Width = length * colCount + 1;
            size.Height = length * rowCount + 1;
            return size;
        }
        #endregion

        #region 线条颜色默认为黑色
        private static Color lineColor = Color.Black;
        public static Color LineColor { get => lineColor; set => lineColor = value; }
        #endregion

        #region 网格背景颜色默认天空蓝色 食物颜色为白色
        private static Color groundColor = Color.LightPink;
        public static Color GroundColor { get => groundColor; set => groundColor = value; }

        private static Color foodcolor = Color.White;
        public static Color FoodColor { get => foodcolor; set => foodcolor = value; }
        #endregion

        #region 玩家的分数和等级
        private static int score;
        public static int Score { get => score; set => score = value; }
        private static int level;
        public static int Level { get => level; set => level = value; }
        #endregion

        #region 蛇的速度
        private static int speed = 600;
        public static int Speed { get => speed; set => speed = value; } 
        #endregion

    }
}
