﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 贪吃蛇小游戏.工具
{
    /// <summary>
    /// 单元格的类型
    /// </summary>
    enum GroundUnitType
    {
        Blank = 0,
        Food = 1
    }
}
