﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace 贪吃蛇小游戏.工具
{
    class SnakeUnit
    {
        #region 身体单元坐标
        //坐标
        private Position pos;
        public Position Pos
        {
            get { return pos; }
            set { pos = value; }
        }
        #endregion
        
        #region 身体单元颜色
        private Color color = Color.Red;
        public Color Color { get => color; set => color = value; }
        #endregion

        #region 以各种构造方法创建蛇的坐标，颜色
        public SnakeUnit(int rowIndex, int colIndex)
        {
            this.pos.rowIndex = rowIndex;
            this.pos.colIndex = colIndex;
        }
        public SnakeUnit(int rowIndex, int colIndex, Color color) : this(rowIndex, colIndex)
        {
            this.Color = color;
        }
        public SnakeUnit(Position pos)
        {
            this.pos = pos;
        }
        public SnakeUnit(Position pos, Color color) : this(pos)
        {
            this.color = color;
        }
        #endregion

        #region 根据当前坐标重新绘制蛇
        public void Draw(Graphics g)
        {
            //对当前所在单元格填充颜色
            g.FillRectangle(new SolidBrush(color), new Rectangle(this.pos.colIndex * CommonHelper.Length + 1, this.pos.rowIndex * CommonHelper.Length + 1, CommonHelper.Length - 1, CommonHelper.Length - 1));
        }
        #endregion

        public void MoveLeft()
        {
            this.pos.colIndex--;
        }
        public void MoveUp()
        {
            this.pos.rowIndex--;
        }
        public void MoveRight()
        {
            this.pos.colIndex++;
        }
        public void MoveDown()
        {
            this.pos.rowIndex++;
        }

        #region 擦除蛇单元 DrawBack()
        public void DrawBack(Graphics g)
        {
            //对当前所在单元格填充颜色
            g.FillRectangle(new SolidBrush(CommonHelper.GroundColor), new Rectangle(this.pos.colIndex * CommonHelper.Length + 1, this.pos.rowIndex * CommonHelper.Length + 1, CommonHelper.Length - 1, CommonHelper.Length - 1));
        }
        #endregion

        #region 蛇的前进方向，默认向左
        private Direction direction = Direction.Left;
        internal Direction Direction { get => direction; set => direction = value; }
        #endregion

        #region 身体移动的旧方向
        private Direction oldDir = Direction.Left;
        internal Direction OldDir { get => oldDir; set => oldDir = value; }

        #endregion
    }
}
