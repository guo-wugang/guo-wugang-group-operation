﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace 贪吃蛇小游戏.工具
{
    class Snake
    {   
        //使用集合
        public LinkedList<SnakeUnit> body = new LinkedList<SnakeUnit>();
        public SnakeUnit SnakeHead
        {
            get
            {
                return this.body.First.Value;
            }
        }
        public Snake()
        {   //创建身体单元，蛇头
            SnakeUnit unit = new SnakeUnit(CommonHelper.RowCount/2,CommonHelper.ColCount/2);
            body.AddFirst(unit);
            AddBody();
            AddBody();
        }
        //往body集合中创建身体对象
        public void AddBody()
        {
            //取最后一个节点的坐标
            Position pos = body.Last.Value.Pos;
            pos.colIndex++;
            SnakeUnit unit = new SnakeUnit(pos,RandomMaker.GetARandomColor());
            body.AddLast(unit);
        }
        //遍历每个单元绘制网格填充
        public void Draw(Graphics g)
        {
            foreach(SnakeUnit u in body)
            {
                u.Draw(g);
            }
        }
        public void AutoSnake(Graphics g)
        {
            //擦除蛇，通过遍历单元
            foreach(SnakeUnit u in body)
            {
                u.DrawBack(g);
            }
            //身体单元前进时坐标改变,通过遍历每个单元都判断方向

            LinkedListNode<SnakeUnit> node = body.First;
            while(node != null)
            {
                switch (node.Value.Direction)
                {
                    case Direction.Left:
                        node.Value.MoveLeft();
                        break;
                    case Direction.Right:
                        node.Value.MoveRight();
                        break;
                    case Direction.Up:
                        node.Value.MoveUp();
                        break;
                    case Direction.Dowm:
                        node.Value.MoveDown();
                        break;
                }
                node.Value.OldDir = node.Value.Direction;
                //设置下一个前进的方向
                if(node.Previous != null)
                {
                    node.Value.Direction = node.Previous.Value.OldDir;
                }
                node = node.Next;
            }
            //foreach (SnakeUnit u in body)
            //{
            //    switch (u.Direction)
            //    {
            //        case Direction.Left:
            //            u.MoveLeft();
            //            break;
            //        case Direction.Right:
            //            u.MoveRight();
            //            break;
            //        case Direction.Up:
            //            u.MoveUp();
            //            break;
            //        case Direction.Dowm:
            //            u.MoveDown();
            //            break;

            //    }
            //}
            //重新绘制蛇
            this.Draw(g);
        }
        ///<summary>
        ///判断蛇头是否自己吃到自己
        /// </summary>
        public bool IsEatSelf()
        {
            //取出蛇头的坐标
            Position pos = this.SnakeHead.Pos;
            //将其他身体遍历作对比
            LinkedListNode<SnakeUnit> node = body.First.Next;
            while (node != null)
            {
                if (node.Value.Pos.rowIndex == pos.rowIndex && node.Value.Pos.colIndex == pos.colIndex)
                {
                    return true;
                }
                node = node.Next;
            }
            return false;
        }
    }
}
