﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace 贪吃蛇小游戏.工具
{
    class RandomMaker
    {
        private static Random random = new Random();
        /// <summary>
        /// 得到一个随机的行坐标
        /// </summary>
        /// <returns></returns>
        private int GetARandomColIndex()
        {
            return random.Next(0, CommonHelper.ColCount);
        }
        private int GetARandomRowIndex()
        {
            return random.Next(0, CommonHelper.RowCount);
        }
        public Position GetARandomPosition()
        {
            Position p = new Position();
            p.rowIndex = this.GetARandomRowIndex();
            p.colIndex = this.GetARandomColIndex();
            return p;
        }
        public static Color GetARandomColor()
        {
            int num = random.Next(0, 10);
            switch (num)
            {
                case 0:
                    return Color.Yellow;
                case 1:
                    return Color.Gray;
                case 2:
                    return Color.Brown;
                case 3:
                    return Color.Pink;
                case 4:
                    return Color.Red;
                case 5:
                    return Color.Gold;
                case 6:
                    return Color.Tomato;
                case 7:
                    return Color.PowderBlue;
                case 8:
                    return Color.Black;
                case 9:
                    return Color.Cornsilk;
                default:
                    return Color.YellowGreen;
            }
        }
    }
}
