﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 随机生成类
    /// </summary>
    class RandomMaker
    {
        private  static Random random =new Random();

        public RandomMaker()
        {
            
        }
        //得到一个随机的横坐标
        public  int  GetRandomX() {
            return random.Next(0,CommerHelper.Xcount);
        }
        //产生一个随机的列坐标
        public int GetRandomY()
        {
            return random.Next(0, CommerHelper.Ycount);
        }
        //产生一个随机的坐标
        public Position GetRandomPosition() { 
             Position p=new Position();
            p.rowIndex = GetRandomX();
            p.colIndex = GetRandomY();
            return p;
        }
        public static Color GetRandomColor() {
            //传入一个随机数
            int num = random.Next(0,10);
            switch (num)
            {
                case 0:
                    return Color.Yellow;
                case 1:
                    return Color.White;
                case 2:
                    return Color.Green;
                case 3:
                    return Color.Brown;
                case 4:
                    return Color.Aqua;
                case 5:
                    return Color.Black;
                case 6:
                    return Color.Chartreuse;
                case 7:
                    return Color.LightGray;
                case 8:
                    return Color.DarkGray;
                default:
                    return Color.Beige;
               
            }
        }
    }
}
