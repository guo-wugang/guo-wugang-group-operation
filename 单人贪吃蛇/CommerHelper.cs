﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 游戏帮助类
    /// </summary>
    static class CommerHelper
    {
        // 定义小方格总行数
        public static int xcount = 11;
        public static int Xcount { get => xcount; set => xcount = value; }

        //游戏面板的列数
        private static int ycount = 13;
        public static int Ycount { get => ycount; set => ycount = value; }

        //小方块的边长

        private static int bian ;
        public static int Bian { get => bian;  }

       

        //游戏数据的方法
        public static Size Data(int panwidth,int panheight) {
            //小方格的宽度
            int Xwidth = panwidth / ycount;
            //小方格的高度
            int Yheight = panheight / xcount;
            //小方块的边长
             bian = Xwidth > Yheight ? Xwidth : Yheight;

            //标准边框的大小
            Size size = new Size();
            size.Width = bian * ycount;
            size.Height = bian * xcount;

            return size;
        }
        //画小方格线的方法
        private static Color linecolor = Color.Black;
        public static Color Linecolor { get => linecolor; set => linecolor = value; }

        //定义画刷的颜色
        private static Color groundcolor = Color.SkyBlue;

        public static Color Groundcolor { get => groundcolor; set => groundcolor = value; }

        //p定义画食物的颜色
        private static Color colorfood=Color.Red;
        public static Color Colorfood { get => colorfood; set => colorfood = value; }
        

        //定义玩家获得的分数
        private static int score;
        public static int Score { get => score; set => score = value; }
       

        //定义玩家的等级
        private static int level;
        public static int Level { get => level; set => level = value; }
        

        //定义蛇运动的速度
        private static int gamespees = 300;
        public static int Gamespees { get => gamespees; set => gamespees = value; }
    }
}
