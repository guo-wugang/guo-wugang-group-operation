﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace 单人贪吃蛇
{
    class Snake
    {
        //由于不知道蛇有多少个身体，所以用户链表来表示蛇的身体，SnakeUnit是蛇身体的类型（也就是从哪里用过来的）
        public LinkedList<SnakeUnit> body = new LinkedList<SnakeUnit>();
        //当我们刚开始创建一个Snake时默认身体单元为0；而我要求有三个，所以用构造函数初始化3个身体单元


        public Snake() {
            SnakeUnit snakeUnit = new SnakeUnit(CommerHelper.Xcount/2,CommerHelper.Ycount/2);
            //将这个身体单元加入到Body里面当做蛇头
            body.AddFirst(snakeUnit);
            Addbody();
            Addbody();
        }
        /// <summary>
        /// 为蛇的身体增加一个元素
        /// </summary>

        public void Addbody() {
            //创建一个身体对象，蛇身的最后一个元素加1；
            //就是行不变，列加1
            // Position po = body.Last.Value.Po;取身体元素的最后一个单元
            //对身体的最后一个单元没有影响，因为po是一个值类型
            Position po = body.Last.Value.Po;
            po.colIndex++;
            SnakeUnit snakeUnit = new SnakeUnit(po,RandomMaker.GetRandomColor());
            body.AddLast(snakeUnit);
        }

        //画蛇
        public void DrawSnake(Graphics g) {

            //通过foreach遍历蛇的身体
            foreach (SnakeUnit u in body) {
                u.DrawMe(g);
            }
        }
        //蛇自己把自己擦掉的方法
        public void WipeMe(Graphics g) {
            //通过foreach遍历蛇擦除后的身体
            foreach (SnakeUnit u in body)
            {
                u.WipeMe(g);
            }

        }
        //蛇移动的方法
        public void Move(Graphics g) {
            //1.擦除这条蛇
            this.WipeMe(g);
            //2.根据上一个单元的移动方向来更改下一个单元
            LinkedListNode<SnakeUnit> node = body.First;
            while (node!=null) {
                switch (node.Value.Dir)
                {
                    case Direction.Left:
                        node.Value.Moveleft();
                        break;
                    case Direction.Up:
                        node.Value.MoveUp();
                        break;
                    case Direction.Right:
                        node.Value.MoveRight();
                        break;
                    case Direction.Down:
                        node.Value.MoveDown();
                        break;
                }
                //将上一个蛇移动的方向保存起来
                node.Value.Olddir = node.Value.Dir;
                //设置下一次蛇身体移动的方向,node.Previous上一个单元
                if (node.Previous!=null) {
                    node.Value.Dir = node.Previous.Value.Olddir;
                }
                node = node.Next;
            }
            ////2.根据每一个身体单元的前进方向来改变坐标，遍历每个身体元素的集合来判断蛇怎么移动
            //foreach (SnakeUnit u in body) {
            //    switch (u.Dir)  {
            //        case Direction.Left:
            //            u.Moveleft();
            //            break;
            //        case Direction.Up:
            //            u.MoveUp();
            //            break;
            //        case Direction.Right:
            //            u.MoveRight();
            //            break;
            //        case Direction.Down:
            //            u.MoveDown();
            //            break;
            //    }
            //}
            //3.重新绘制这条蛇
            this.DrawSnake(g);
        }
        //取出蛇头坐标
        public SnakeUnit Snakehead {
            get { return this.body.First.Value; }
        }
        //判断蛇头是否碰到自己的身体
        public bool IsEatSelf() {
            //取出蛇头坐标
            Position pos = this.Snakehead.Po;
            //将身体的单元遍历出来，与蛇头的坐标比较
            LinkedListNode<SnakeUnit> node = body.First.Next;
            while (node!=null) {
                if (pos.rowIndex == node.Value.Po.rowIndex && pos.colIndex == node.Value.Po.colIndex) {
                    return true;
                }
                 node = node.Next;
            
            }
            return false;
        }
        
        
    }
}
