﻿
namespace 单人贪吃蛇
{
    partial class MainFous
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Btnstart = new System.Windows.Forms.Button();
            this.Gamepanle = new System.Windows.Forms.Panel();
            this.Btnscore = new System.Windows.Forms.Label();
            this.Btngrade = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btnstart
            // 
            this.Btnstart.Location = new System.Drawing.Point(26, 10);
            this.Btnstart.Name = "Btnstart";
            this.Btnstart.Size = new System.Drawing.Size(107, 52);
            this.Btnstart.TabIndex = 0;
            this.Btnstart.Text = "开始游戏";
            this.Btnstart.UseVisualStyleBackColor = true;
            this.Btnstart.Click += new System.EventHandler(this.Btnstart_Click);
            // 
            // Gamepanle
            // 
            this.Gamepanle.Location = new System.Drawing.Point(2, 86);
            this.Gamepanle.Name = "Gamepanle";
            this.Gamepanle.Size = new System.Drawing.Size(523, 425);
            this.Gamepanle.TabIndex = 1;
            this.Gamepanle.Paint += new System.Windows.Forms.PaintEventHandler(this.Gamepanle_Paint);
            // 
            // Btnscore
            // 
            this.Btnscore.BackColor = System.Drawing.Color.Red;
            this.Btnscore.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Btnscore.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btnscore.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Btnscore.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Btnscore.Location = new System.Drawing.Point(169, 20);
            this.Btnscore.Name = "Btnscore";
            this.Btnscore.Size = new System.Drawing.Size(123, 39);
            this.Btnscore.TabIndex = 2;
            this.Btnscore.Text = "分数：0";
            // 
            // Btngrade
            // 
            this.Btngrade.BackColor = System.Drawing.Color.Red;
            this.Btngrade.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Btngrade.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Btngrade.Location = new System.Drawing.Point(350, 20);
            this.Btngrade.Name = "Btngrade";
            this.Btngrade.Size = new System.Drawing.Size(123, 39);
            this.Btngrade.TabIndex = 3;
            this.Btngrade.Text = "等级：0";
            // 
            // MainFous
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 509);
            this.Controls.Add(this.Btngrade);
            this.Controls.Add(this.Btnscore);
            this.Controls.Add(this.Gamepanle);
            this.Controls.Add(this.Btnstart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainFous";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "单人贪吃蛇";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button Btnstart;
        private System.Windows.Forms.Panel Gamepanle;
        private System.Windows.Forms.Label Btnscore;
        private System.Windows.Forms.Label Btngrade;
    }
}

