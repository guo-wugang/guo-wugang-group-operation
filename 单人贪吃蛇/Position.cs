﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 蛇身体的坐标
    /// </summary>
    ///struct结构体
    ///结构体不能直接去改变它属性的值，必须通过new一个新的对象来改变，变为字段是可以的，属性不行
    struct Position
    {
        //x坐标
        public int rowIndex;
        //y坐标
        public int colIndex;
        //Position的构造函数
        public Position(int rowIndex,int colIndex)
        {
            this.rowIndex = rowIndex;
            this.colIndex = colIndex;
        }
    }
}
