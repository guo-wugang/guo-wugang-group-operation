﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 蛇身体单元的类
    /// </summary>
    class SnakeUnit
    {
        //坐标
        private Position po;
        public Position Po { get => po; set => po = value; }
        //蛇身体的颜色
        private Color color = Color.Red;

        public Color Color { get => color; set => color = value; }
        
       

        //用一个构造函数来接受行和列坐标
        public SnakeUnit(int rowIndex, int colIndex) {
            this.po.rowIndex = rowIndex;
            this.po.colIndex = colIndex;
        }
        //函数的重载
        public SnakeUnit(int rowIndex, int colIndex,Color color)
            :this(rowIndex, colIndex)
       
        {
            this.color = color;
        }
        public SnakeUnit(Position po) {
            this.po = po;
        }
        public SnakeUnit(Position po, Color color) : this(po) {
            this.color = color;
        }

       

        //身体单元自己画自己
        public void DrawMe(Graphics g) {
            Brush brush = new SolidBrush(this.color);
            g.FillRectangle(brush,new Rectangle(this.po.colIndex*CommerHelper.Bian+1,
                this.po.rowIndex*CommerHelper.Bian+1,CommerHelper.Bian-1,
                CommerHelper.Bian-1)
                );
        }
        //默认方向向左移动
        private Direction dir = Direction.Left;
        internal Direction Dir { get => dir; set => dir = value; }
        //蛇头上一次移动的方向
        private Direction olddir = Direction.Left;
        internal Direction Olddir { get => olddir; set => olddir = value; }

        //身体单元向左移动,列坐标减1
        public void Moveleft() {
            this.po.colIndex--;
        }
        //向上移动，行坐标减1
        public void MoveUp() {
            this.po.rowIndex--;
        }
        //向右移动，列坐标加一
        public void MoveRight() {
            this.po.colIndex++;
        }
        //向下移动，行坐标加1
        public void MoveDown() {
            this.po.rowIndex++;
        }
        //小方格自己把自己擦掉的方法
        public void WipeMe(Graphics g) {
            Brush brush = new SolidBrush(CommerHelper.Groundcolor);
            g.FillRectangle(brush, new Rectangle(this.po.colIndex * CommerHelper.Bian + 1,
                this.po.rowIndex * CommerHelper.Bian + 1, CommerHelper.Bian - 1,
                CommerHelper.Bian - 1)
                );
        }
        
        

    }
}
