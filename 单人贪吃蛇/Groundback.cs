﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    class Groundback
    {
        //二维数组用来表示背景单元格的类型
        //二维数组每一个元素的值对应地图上每一个格子的类型
        //blocks[3,2]代表地图上第3行第2列的格子的类型
        //在创建完Ground后，每一个格子的类型就是Blank（值类型，默认为0，就是Blank类型)
        private SnakeUnitType[,] blocks = new SnakeUnitType[CommerHelper.Ycount,CommerHelper.Xcount];
        //产生一个随机生成器
        private RandomMaker randomMaker = new RandomMaker();
        //产生一个随机坐标，将方格画在地图上
        public void MakeFood(Graphics g,Snake snake) {
        loop:
            //产生一个随机的坐标
            Position position = randomMaker.GetRandomPosition();
            Console.WriteLine("随机产生的坐标：{0},{1}",position.colIndex,position.rowIndex);
            //判断这个食物是否在蛇的身体上面，在的话就重新产生
            //1.首先将蛇身体内的每一个元素遍历出来然后与蛇的身体进行比较，只要有一个相同，就得从新参生
            foreach (SnakeUnit u in snake.body) {
                if (u.Po.rowIndex==position.rowIndex&&u.Po.colIndex==position.colIndex) {
                    Console.WriteLine("产生的坐标重复，重新产生");
                    goto loop;
                }
            }
            Console.WriteLine("产生的坐标没有重复");
            //将二维数组的值改为食物
            blocks[position.colIndex, position.rowIndex] = SnakeUnitType.Food;
            //在界面上面将食物画出来
            Brush brush = new SolidBrush(Color.Red);
            g.FillRectangle(brush,new Rectangle(position.colIndex*CommerHelper.Bian+1,
                position.rowIndex*CommerHelper.Bian+1, 
                CommerHelper.Bian-1, 
                CommerHelper.Bian-1)
                );
        }
        //画背景的方法
        public void Draw(Graphics g) {

            
            //利用嵌套循环来画表格
            //i表示行数
            //a表示列数
            for (int i = 0; i < CommerHelper.Xcount; i++)
            {
                for (int a = 0; a < CommerHelper.Ycount; a++)
                {
                    Pen pen = new Pen(CommerHelper.Linecolor);
                    //让线程停一下可以看到效果
                    //Thread.Sleep(200);
                    g.DrawRectangle(pen, new Rectangle(a *CommerHelper.Bian, i*CommerHelper.Bian,CommerHelper.Bian, CommerHelper.Bian));
                    //给小方格填充颜色,由于填充色会覆盖小方格，所以位置+1；长度-1
                    SolidBrush solidBrush = new SolidBrush(CommerHelper.Groundcolor);
                    g.FillRectangle(solidBrush, new Rectangle(a * CommerHelper.Bian + 1, i * CommerHelper.Bian + 1, CommerHelper.Bian - 1, CommerHelper.Bian - 1));
                }

            }
        }
        //判断蛇食物是否移动
        public bool IsMoveable(Snake snake) {
            //取出蛇头的坐标
            Position pos = snake.Snakehead.Po;
            //根据蛇头的坐标算出前进的下一个坐标
            switch (snake.Snakehead.Dir)
            {
                case Direction.Left:
                    
                    pos.colIndex--;
                    break;
                case Direction.Up:
                    pos.rowIndex--;
                    break;
                case Direction.Right:
                    pos.colIndex++;
                    break;
                case Direction.Down:
                    pos.rowIndex++;
                    break;
              
            }
            //判断蛇头的下一个坐标是否越界
            if (pos.colIndex<0||pos.rowIndex<0||pos.rowIndex>=CommerHelper.Xcount||pos.colIndex>=CommerHelper.Ycount
                ) {
                return false;
            }
            return true;
        }
        //判断蛇是否吃到了食物
        public bool IsEatFood(Snake snake) {
            //取出蛇头的坐标
            Position pos = snake.Snakehead.Po;
            //以蛇头的坐标当做二维数组的下标，来判断对应的方格类型
            SnakeUnitType type = blocks[pos.colIndex, pos.rowIndex];
            //判断如果吃到的食物，就返回ture
            if (type == SnakeUnitType.Food)
            {
                //产生食物以后把食物的元素取消了
                blocks[pos.colIndex, pos.rowIndex] = SnakeUnitType.Blank;
                return true;
            }
            else return false;
        }
    }
}
