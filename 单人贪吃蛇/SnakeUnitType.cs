﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 定义枚举单元格的类型
    /// </summary>
    enum SnakeUnitType
    {
        //普通的背景方格
        Blank=0,
        //食物的背景方格
        Food=1,
        //障碍物的背景方格
        Obstacle=2
    }
}
