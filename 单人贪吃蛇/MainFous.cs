﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 单人贪吃蛇
{
    public partial class MainFous : Form
    {
        //游戏控制器对象
        private CameController cameController;
        public MainFous()
        {
            InitializeComponent();
            //获得小方块的边长 
            this.Gamepanle.Size= CommerHelper.Data(this.Gamepanle.Width,this.Gamepanle.Height);
            //关闭跨线程访问检查
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Btnstart_Click(object sender, EventArgs e)
        {
            BtOk btOk = new BtOk();
            //btOk.Visible = true;
            DialogResult dialog =btOk.ShowDialog();
            if (dialog ==DialogResult.OK) {
                MessageBox.Show("登录成功");
            }
            
            //1.点击开始游戏后再画板上画开始界面，在panale上面创建画笔对象
            //由于在Gamepanle上面定义的画笔，所以只能在Gamepanle上面画画
            Graphics g = Graphics.FromHwnd(this.Gamepanle.Handle);
            //不用和蛇和背景类联系，直接和游戏控制器关联（因为游戏控制器当中包括蛇和背景类）

            cameController = new CameController(g,this);
            cameController.Newgame();
            Btnstart.Enabled = false; 
            
            

        }
        //这个方法当我们在键盘上按下任意键的时候，都会调用这个方法
        //keyData会得到用户在键盘上按下的键
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (cameController != null)
            {
                //将用户传入的键传给控制器
                this.cameController.ProcessDialogKey(keyData);
            }
            return base.ProcessDialogKey(keyData);
        }
        //显示玩家等级和分数的方法
        public void ShowScoreAndLevel() {
            Btnscore.Text = "分数："+CommerHelper.Score;
            Btngrade.Text = "等级：" + CommerHelper.Level;

        }

        private void Gamepanle_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
