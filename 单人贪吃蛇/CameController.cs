﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 用来控制整个游戏的逻辑
    /// </summary>
    class CameController
    {
        //游戏控制器来控制的背景和蛇对象
        private Snake snake;
        private Groundback groundback;
        //画家对象字段（由游戏面板的画家对象传入）
        private Graphics graphics;
        //传入的界面对象
        private MainFous fou;
        /// <summary>
        /// 构造函数用来初始化蛇和背景
        /// </summary>
        public CameController(Graphics g,MainFous fous) {
            this.graphics = g;
            this.snake = new Snake();
            this.groundback = new Groundback();
            this.fou = fous;
        }

        public void Newgame() {
            //重新开始游戏后将等级，分数以及蛇的速度都变成0
            CommerHelper.Score =0;
            CommerHelper.Level =0;
            CommerHelper.Gamespees = 300;
            fou.ShowScoreAndLevel();
            //绘制背景板
            this.groundback.Draw(graphics);
            //产生一个食物
            this.groundback.MakeFood(graphics,snake);
            //绘制蛇
            this.snake.DrawSnake(graphics);
            //让蛇运动的线程开始
            Thread start = new Thread(MoveSnakeThread);
            //start.IsBackground = true;让他的后台线程一直执行
            start.IsBackground = true;
            start.Start();
            
        }
        //应该把蛇动起来放到一个线程里面
        private void MoveSnakeThread() {
            while (groundback.IsMoveable(snake)) {
                //如果蛇头吃到自己的身体则游戏结束
                if (snake.IsEatSelf()) {
                    break;
                }
                //判断是否吃到了食物，如果吃到，身体加一
                if (groundback.IsEatFood(snake)) {
                    snake.Addbody();
                    groundback.MakeFood(graphics, snake);
                    Thread.Sleep(500);
                    //加等级
                    CommerHelper.Score++;
                    //加等级
                    if (CommerHelper.Score % 5 == 0) {
                        CommerHelper.Level++;
                        CommerHelper.Gamespees -= 50;
                    }
                    //显示分数
                    fou.ShowScoreAndLevel();
                    
                }
                snake.Move(graphics);
                Thread.Sleep(CommerHelper.Gamespees);
            }
            //否则游戏结束
            graphics.DrawString("GameOver",new Font("宋体",50),Brushes.Yellow,new Point(16,16));
            //游戏结束以后将开始游戏按钮显示出来
            //注意：必须将MainFous.Designer.cs中的按钮改为public
            fou.Btnstart.Enabled = true;
        }

        /// <summary>
        /// 处理用户传入的键
        /// </summary>
        /// <param name="keyData"></param>
        public void ProcessDialogKey(Keys keyData) {
            switch (keyData)
            {
                case Keys.Up:
                    snake.Snakehead.Dir = Direction.Up;
                    break;
                case Keys.Down:
                    snake.Snakehead.Dir = Direction.Down;
                    break;
                case Keys.Left:
                    snake.Snakehead.Dir = Direction.Left;
                    break;
                case Keys.Right :
                    snake.Snakehead.Dir = Direction.Right;
                    break;
                
            }
        }
    }
}
