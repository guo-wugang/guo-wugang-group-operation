﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 单人贪吃蛇
{
    /// <summary>
    /// 蛇前进方向的枚举
    /// </summary>
    enum Direction
    {
        //左
        Left=0,
        //上
        Up=1,
        //右
        Right=2,
        //下，枚举最后一个不应该有逗号
        Down=3

    }
}
